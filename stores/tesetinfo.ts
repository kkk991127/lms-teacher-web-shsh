import {acceptHMRUpdate, defineStore} from "pinia";

interface TestRequest{
  subjectId : number
  teacherId : number
  abilityUnitFactorName : string
  abilityUnitName : string
  testType : string
  problemNum : number
  testTime : string
  testName : string
  score : number
  level : number
  dateTest : string
  ncs : string
}

interface TestDetail{
  subjectName : string
  abilityUnitName :string
  abilityUnitFactorName : string
  teacherName :string
  testName : string
  dateTest :string
  testTime : number
  testType : string
  isSetTest : string
  level : number
  registerHuman : number
  score : number
  problemNum : number
  ncs : number
}

interface  makeTest{
  subjectStatusId : number
  testProblem : string
  testSelect : string
}

interface  makeTestList{
  studentName : string
  makeTest : string
  testStatus : string
}

interface answerTestDetail{
  makeTestName : string
  studentName : string
  problem : string
  testAnswer : string
  addFile : string
}
interface answerTestRequest{
  teacherComment : string
  studentScore : number
  achieveLevel : number

}

export const useTestStore = defineStore('testInfo', {
  state: () => ({
    testInfoData:[] as TestRequest[],
    testDetail:[] ,
    makeTestData: [] as makeTest[],
    makeTestList: [] as makeTestList[],
    answerTestDetail : [] as answerTestDetail[],
    subjectName: [],
    testName:[],
    currentPage: 1,
    pageSize: 10,
    totalItems: 0,
    totalPage: 0,
    totalCount:0,
  }),


  actions: {
    setAnswerTest(data: answerTestRequest) {
      const {id} = useRoute().params
      $fetch(`http://34.64.218.128:8080/v1/answer-test/putScore/answerTestId/${id}`,{
        method:'PUT',
        body : data
      });
      console.log(data)
      console.log('hi')
    },


    // 시험 정보 등록하기
    setTestInfo(data: TestRequest) {
      const token = useCookie('token');
      console.log(token.value)
      $fetch('http://34.64.218.128:8080/v1/subjectStatus/new',{
        method:'POST',
        body : data,
        headers: {
          'Authorization': `Bearer ${token.value}`
        }
      });
      console.log(data)
      console.log('hi')
    },
    // 시험 문제 출제하기
    setMakeTest(data: makeTest) {

      $fetch('http://34.64.218.128:8080/v1/makeTest/new',{
        method:'POST',
        body : data
      });
      console.log(data)
      console.log('hi')
    },
    // 시험지 채점 할 때 리스트 불러오는
    async getAnswerList() {
      try {
        const {id} = useRoute().params
        const { data }: any = await useFetch(

          `http://34.64.218.128:8080/v1/answer-test/all/${id}`,
          {
            method: 'GET',
          }
        );
        if (data) {
          this.makeTestList = data.value.list;
          console.log(data.value.list);
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    },

    // 시험지 채점 리스트에서 상세보기 문제 ,답변 불러오는
    async getAnswerDetailList(id:string) {
      try {
        const {id} = useRoute().params
        const { data }: any = await useFetch(
          `http://34.64.218.128:8080/v1/answer-test/detail/answerTestId/${id}`,
          {
            method: 'GET',
          }
        );
        if (data) {
          this.answerTestDetail = data.value;
          console.log(data.answerTestDetail)
        }
      } catch (error) {
        console.error('시험지 채점 상세보기 오류입니다 !!!', error);
      }
    },



    // 시험정보 리스트 가져오기.
    async getTestInfoList() {
      try {
        const { data }: any = await useFetch(
          `http://34.64.218.128:8080/v1/subjectStatus/all`,
          {
            method: 'GET',
          }
        );
        if (data) {
          this.testInfoData = data.value.list;
          console.log(data.value.list);

        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    },
    // 시험 정보 상세보기
    async getTestInfoDetail(subjectStatusId:string){
      const {id} = useRoute().params
      const {data}: any = await useFetch(
        `http://34.64.218.128:8080/v1/subjectStatus/detail/subjectStatusId/${id}`,
        {
          method: 'GET',
        });
      if (data) {

        this.testDetail = data.value;
      }
    },
    // 시험 정보 페이징
    async getTestPaging(pageNum:number, pageSize:number){
      const token = useCookie('token');
      const {data}: any = await useFetch(
        `http://34.64.218.128:8080/v1/subjectStatus/all/subject-id/1/pageNum/1`,
        {
          method: 'GET',
        });
      if (data) {
        this.testInfoData = data.value.list;
      }
    },

    // 시험 정보 수정하기
    async editTestInfo(data: TestRequest) {
      const token = useCookie('token');
      const {id} = useRoute().params
      await useFetch(
        `http://34.64.218.128:8080/v1/subjectStatus/changeInfo/subjectStatusId/${id}`,
        {
          method: 'PUT',
          body: data,
        }
      );
    }
  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useTestStore, import.meta.hot))
}

