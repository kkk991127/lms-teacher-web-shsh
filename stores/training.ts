import {acceptHMRUpdate, defineStore} from "pinia";

interface trainingRequest {
  subjectId : number
  dateTraining:string
  registrationNum : number
  attendanceNum : number
  theory: number
  practice : number
  absentWho : string[]
  lateWho : string[]
  earlyLeaveWho : string[]
  trainingContent : string
  etc : string
}

interface trainingDetail{
  dateTraining : string
  registrationNum : number
  attendanceNum : number
  theory : number
  practice : number
  absentWho : string[]
  lateWho : string[]
  earlyLeaveWho : string[]
  trainingContent : string
  etc : string
}


// 데이터의 타입을 지정해줘야함
// auth 스토어

export const useTrainingStore = defineStore('training', {
  state: () => ({
    trainingData: [] as trainingRequest[],
    trainingDetail:[] as trainingDetail[],
  }),
  getters: {
  },

  actions: {
    async setTraining(data: trainingRequest) {
      const token = useCookie('token');
      console.log(token.value)
      // 배포 스웨거 주소
      await useFetch('http://34.64.218.128:8080/v1/trainingRegister/new',{
        method:'POST',
        body : data,
        headers: {
          'Authorization': `Bearer ${token.value}`
        }
      });
      console.log('hello')
      console.log(data)
    },
    async getTrainingList(subjectId:number) {
      try {
        const { data }: any = await useFetch(
          `http://34.64.218.128:8080/v1/trainingRegister/all/subjectId/${subjectId}`,
          {
            method: 'GET',
          }
        );
        if (data) {
          this.trainingData = data.value.list;
          console.log(data.value.list);

        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    },
    async getTrainingDetail(trainingRegisterId:string) {
      const {id} = useRoute().params
      try {
        const { data }: any = await useFetch(
          `http://34.64.218.128:8080/v1/trainingRegister/detail/trainingRegisterId/${id}`,
          {
            method: 'GET',
          }
        );
        if (data) {
          this.trainingDetail = data.value;
          console.log(data.trainingDetail);

        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    },
  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useTrainingStore, import.meta.hot))
}

