// http://192.168.0.215:8080/v1/student/all
import {acceptHMRUpdate, defineStore} from "pinia";

// 훈련생 리스트 받아오기
interface studentRequest {
  studentName : string
  subjectId: string
  imgSrc : string
  gender : string
  dateBirth : string
  studentStatus : string
  statusWhy : string
  lateWho : string
}
interface studentDetail{
  id: number
  subjectChoice: string
  imgSrc: string
  studentName: string
  gender: string
  dateBirth: string
  phoneNumber: string
  idNumber: string
  address: string
  email: string
  finalEducation:string
  major: string
  studentStatus: string
  dateStatusChange: string
  statusWhyAndDate: string
  dateJoin: string
}



// 데이터의 타입을 지정해줘야함
// auth 스토어

export const useStudentStore = defineStore('tableData', {
  state: () => ({
    tableData: [],
    tableDetail: [] as studentDetail[]

  }),

  actions: {
    async getList() {
      const {data}: any = await useFetch(
        `http://34.64.218.128:8080/v1/student/all`, {
          method: 'GET',
        });
      if (data) {
        this.tableData = data.value.list;
        console.log(this.tableData, 3)

      }
    },
    // 수강생페이징
    async getPaging(pageNum:number, pageSize:number){
      const token = useCookie('token');
      const {data}: any = await useFetch(
        `http://34.64.218.128:8080/v1/student/all/pageNum/${pageNum}`,
        {
          method: 'GET',
        });
      if (data) {
        this.tableData = data.value.list;
      }
    },
    // todo : 새로고침 해야 나옴.
    async getDetailList(detailId: String) {
      const { id } = useRoute().params
      const {data}: any = await useFetch(

        `http://34.64.218.128:8080/v1/student/detail/studentId/${id}`,
        {
          method: 'GET',
        });
      if (data) {
        console.log(id,10)
        this.tableDetail = data.value;
      }
    },
  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useStudentStore, import.meta.hot))
}

