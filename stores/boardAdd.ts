import {acceptHMRUpdate, defineStore} from "pinia";


interface IBoardRequest {
  teacherId : number
  studentId : number
  boardTitle: string
  boardWriter: string
  boardContent: string
}
// 데이터의 타입을 지정해줘야함
// auth 스토어

export const useBoardStore = defineStore('auth', {
  state: () => {
    return{

    }
  },
  getters: {
  },

  actions: {
    setUser(data: IBoardRequest) {
      $fetch('http://192.168.0.151:8080/v1/board/new',{
        method:'POST',
        body : data
      });
      console.log('help')
    }

  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useBoardStore, import.meta.hot))
}
