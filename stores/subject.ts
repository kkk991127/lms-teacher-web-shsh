import {acceptHMRUpdate, defineStore} from "pinia";
interface subjectRequest{
  id : number
  subjectId : number
  teacherId : number
  subjectName : string
  registerHuman :number
  abilityUnitName : string
  abilityUnitFactorName : string
  teacherName : string
  testName : string
  dateTest : string
  testTime : string
  testType : string
  isSetTest :string
}
interface subjectDetail{
  subjectName : string
  abilityUnitName :string
  abilityUnitFactorName : string
  teacherName :string
  testName : string
  dateTest :string
  testTime : number
  testType : string
  isSetTest : string
  level : number
  registerHuman : number
  score : number
  problemNum : number
  ncs : number
}
export const useSubjectStore = defineStore('subject', {
  state: () => ({
    subjectData: [] as subjectRequest[],
    subjectDetail: [] as subjectDetail []
  }),
  actions: {
    async getSubjectList(){
      const {data}: any = await useFetch(
        `http://34.64.218.128:8080/v1/subject/all`, {
          method: 'GET',
        });
      if (data) {
        this.subjectData = data.value.list;
        console.log(data.value.list,"1111111111111111111111111111111111")
      }
    },
    async getPaging(pageNum:number, pageSize:number){
      const token = useCookie('token');
      const {data}: any = await useFetch(
        `http://34.64.218.128:8080/v1/subjectStatus/all/pageNum/${pageNum}`,
        {
          method: 'GET',
        });
      if (data) {
        this.subjectData = data.value.list;
      }
    },
    async getSubjectDetail(detailId:string){
      const {id} = useRoute().params
      const {data}: any = await useFetch(
        `http://34.64.218.128:8080/v1/subjectStatus/detail/subjectStatusId/${id}`,
        {
          method: 'GET',
        });
      if (data) {
        this.subjectDetail = data.value;
      }
    }
  }
})
if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useSubjectStore, import.meta.hot))
}
