export default [
  {
    isTitle: true,
    name: "Menu",
    url: "",
    icon: "",
    submenu: [],
  },

  {
    isTitle: false,
    name: "강사 대시보드",
    url: "/",
    icon: "user",
    submenu: [],
  },

  {
    isTitle: false,
    name: "학생관리",
    key: "forms",
    icon: "users",
    submenu: [

      {
        isTitle: false,
        name: "훈련생 현황",
        url: "/table/datatable",
        icon: "chevron-right",
        submenu: [
        ],
      },
      {
        isTitle: false,
        name: "취업관리",
        url: "/employManagement/list",
        icon: "users",
        submenu: [],
      },
      {
        isTitle: false,
        name: "상담관리",
        url: "/advice/advicelist",
        icon: "users",
        submenu: [],
      },
    ],
  },

  {
    isTitle: false,
    name: "훈련관리",
    url: "",
    icon: "",
    submenu: [
      {
        isTitle: false,
        name: "훈련일지 등록",
        url: "/training/trainingadd",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "훈련일지",
        url: "/training/traininglist",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "출결관리",
        url: "/attendance/list",
        icon: "chevron-right",
        submenu: [],
      },
    ],
  },

  {
    isTitle: false,
    name: "평가관리",
    url: "",
    icon: "",
    submenu: [
      {
        isTitle: false,
        name: "평가관리",
        url: "/test/datatable",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "시험정보 등록",
        url: "/test/testinfoc",
        icon: "chevron-right",
        submenu: [],
      },

    ],
  },
  {
    isTitle: false,
    name: "상담 관리",
    key: "forms",
    icon: "message-square",
    submenu: [
      {
        isTitle: false,
        name: "상담 관리",
        url: "/advice/advicelist",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "취업 관리",
        url: "/employManagement/list",
        icon: "chevron-right",
        submenu: [],
      },

    ],
  },

  {
    isTitle: true,
    name: "서류관리",
    url: "",
    icon: "users",
    submenu: [],
  },
  {
    isTitle: false,
    name: "강사용 서류",
    key: "forms",
    icon: "users",
    submenu: [
      {
        isTitle: false,
        name: "General",
        url: "/form/general",
        icon: "chevron-right",
        submenu: [],
      },
    ],
  },
  {
    isTitle: false,
    name: "훈련생 정보",
    key: "table",
    icon: "users",
    submenu: [
      {
        isTitle: false,
        name: "훈련생 list",
        url: "/table/datatable",
        icon: "chevron-right",
        submenu: [],
      },
    ],
  },



];

